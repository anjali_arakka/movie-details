public class Movie {


    private final String title ;
    private final String studio ;
    private final String rating ;


    public Movie(String movieTitle, String movieStudio, String movieRating) {
        title = movieTitle ;
        studio = movieStudio ;
        rating = movieRating ;

    }

    public  Movie(String movieTitle, String movieStudio) {
        title = movieTitle ;
        studio = movieStudio ;
        rating = "PG" ;

    }

    public static Movie[] getPG(Movie[] array) {

        Movie[] pgArray = new Movie[array.length] ;

        int j=0 ;

        for (Movie movie : array) {
            if (movie.rating.equals("PG")) {
                pgArray[j++] = movie;
            }


        }

        return pgArray;
    }


    @Override
    public String toString() {
        return "title = " + title + "\nstudio = " + studio + "\nrating = " + rating ;
    }



    public static void main(String[] arg) {

        Movie film =  new Movie("Casino Royale", "Eon Productions", "PG-13" ) ;
        System.out.println(film);

        System.out.println();

        Movie film2 = new Movie("Dr. Strange", "Marvel Studios") ;
        System.out.println(film2);


        Movie[] filmSeries = new Movie[4] ;
        filmSeries[0] = new Movie("eclipse", "Animation Productions") ;
        filmSeries[1] = new Movie("Radiation", "Avon Productions") ;

        filmSeries[2] = new Movie("Rose", "Sky Pictures", "58") ;
        filmSeries[3] = new Movie("Furious", "Unlimited Studios", "75") ;



        for (Movie filmList : filmSeries) {

            if (filmList != null)
                System.out.println(filmList);

        }


        Movie[] pgArray = getPG(filmSeries) ;

        for (Movie movie : pgArray) {

            if (movie != null)
                System.out.println(movie);

        }



    }
}
